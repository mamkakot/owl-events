export type UserData = {
  birthDate: Date;
  email: string;
  educationalInstitutions?: EducationalInstitution[];
  currentEducationalInstitution?: EducationalInstitution;
  previousNames?: UserName[];
  currentName: UserName;
  iacId?: string;
  password: string;
  aboutMe?: string;
  teamName?: string;
}

type UserName = {
  firstName: string;
  secondName: string;
  patronymicName?: string | null;
}

type EducationalInstitution = {
  name: string;
  educationalInstitutionType?: EducationalInstitutionType;
  dateStartGraduation?: Date;
  dateEndGraduation?: Date;
}

type EducationalInstitutionType = {
  name: string;
}
