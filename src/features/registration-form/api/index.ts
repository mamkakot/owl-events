import { supabase } from '@/shared/api';
import type { UserData } from '@/features/registration-form';

export async function registerUser(newUser: UserData) {
  // Регистрация пользователя в supabase.
  const signUpResult = await supabase.auth.signUp({
    email: newUser.email,
    password: newUser.password,
  });

  if (signUpResult.error) {
    console.error(`sign up error: ${signUpResult.error}`);
    throw signUpResult.error;
  }

  console.log('auth success');

  // Создание записи в таблице с именами пользователей.
  const createUserNameResult = await supabase
    .from('userNames')
    .insert({
      firstName: newUser.currentName.firstName,
      secondName: newUser.currentName.secondName,
      patronymicName: newUser.currentName.patronymicName,
    })
    .select();

  if (createUserNameResult.error) {
    throw createUserNameResult.error;
  }

  // Создание записи в таблице с учебными заведениями.
  const createEducationalInstitutionResult = await supabase
    .from('educationalInstitutions')
    .insert({
      educationalInstitutionTypeId: 1,
      name: newUser.currentEducationalInstitution!.name,
    })
    .select();

  if (createEducationalInstitutionResult.error) {
    throw createEducationalInstitutionResult.error;
  }

  // Создание пользователя в домене проекта.
  const createUserResult = await supabase
    .from('users')
    .insert({
      authUserId: signUpResult.data.user!.id,
      birthDate: newUser.birthDate.toLocaleDateString(),
    })
    .select();

  if (createUserResult.error) {
    throw createUserResult.error;
  }

  // Создание записи в таблице с учебными заведениями пользователя.
  const createUserEducationalInstitutionResult = await supabase
    .from('usersEducationalInstitutions')
    .insert({
      userId: createUserResult.data[0].id,
      educationalInstitutionId: createEducationalInstitutionResult.data[0].id,
      isCurrent: true,
    });

  if (createUserEducationalInstitutionResult.error) {
    throw createUserEducationalInstitutionResult.error;
  }

  // Создание записи в таблице с соответствием имён и пользователей, проставление текущего имени пользователя.
  const createUserCurrentNameResult = await supabase.from('allUserNames').insert({
    userId: createUserResult.data[0].id,
    userNameId: createUserNameResult.data[0].id,
    isCurrent: true,
  });

  if (createUserCurrentNameResult.error) {
    throw createUserCurrentNameResult.error;
  }
}