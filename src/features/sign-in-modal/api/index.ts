import { supabase } from '@/shared/api';
import type { SignInData } from '../model/signInData.types';

export async function signInUser(signInData: SignInData) {
  // Регистрация пользователя в supabase.
  const signInResult = await supabase.auth.signInWithPassword({
    email: signInData.email,
    password: signInData.password,
  });

  if (signInResult.error) {
    console.error(`sign in error: ${signInResult.error}`);
    throw signInResult.error;
  }

  console.log('sign in success');
  return signInResult.data.user;
}