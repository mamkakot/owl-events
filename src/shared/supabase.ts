export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export type Database = {
  public: {
    Tables: {
      addresses: {
        Row: {
          apartment: string | null
          building: string
          buildingAffix: string | null
          city: string
          country: string
          county: string | null
          createdAt: string
          id: string
          street: string
        }
        Insert: {
          apartment?: string | null
          building: string
          buildingAffix?: string | null
          city: string
          country: string
          county?: string | null
          createdAt?: string
          id?: string
          street: string
        }
        Update: {
          apartment?: string | null
          building?: string
          buildingAffix?: string | null
          city?: string
          country?: string
          county?: string | null
          createdAt?: string
          id?: string
          street?: string
        }
        Relationships: []
      }
      allUserNames: {
        Row: {
          id: number
          isCurrent: boolean
          userId: number
          userNameId: string
        }
        Insert: {
          id?: number
          isCurrent?: boolean
          userId: number
          userNameId: string
        }
        Update: {
          id?: number
          isCurrent?: boolean
          userId?: number
          userNameId?: string
        }
        Relationships: [
          {
            foreignKeyName: "allUserNames_userId_fkey"
            columns: ["userId"]
            isOneToOne: false
            referencedRelation: "users"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "allUserNames_userNameId_fkey"
            columns: ["userNameId"]
            isOneToOne: false
            referencedRelation: "userNames"
            referencedColumns: ["id"]
          },
        ]
      }
      basicTeamStructures: {
        Row: {
          createdAt: string
          id: number
          isCaptain: boolean
          playerId: number
          teamId: string
        }
        Insert: {
          createdAt?: string
          id?: number
          isCaptain?: boolean
          playerId: number
          teamId: string
        }
        Update: {
          createdAt?: string
          id?: number
          isCaptain?: boolean
          playerId?: number
          teamId?: string
        }
        Relationships: [
          {
            foreignKeyName: "basic_team_structures_player_id_fkey"
            columns: ["playerId"]
            isOneToOne: false
            referencedRelation: "users"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "basic_team_structures_team_id_fkey"
            columns: ["teamId"]
            isOneToOne: false
            referencedRelation: "teams"
            referencedColumns: ["id"]
          },
        ]
      }
      educationalInstitutions: {
        Row: {
          createdAt: string
          educationalInstitutionTypeId: number | null
          id: string
          name: string
        }
        Insert: {
          createdAt?: string
          educationalInstitutionTypeId?: number | null
          id?: string
          name: string
        }
        Update: {
          createdAt?: string
          educationalInstitutionTypeId?: number | null
          id?: string
          name?: string
        }
        Relationships: [
          {
            foreignKeyName: "educational_institutions_educational_institution_type_id_fkey"
            columns: ["educationalInstitutionTypeId"]
            isOneToOne: false
            referencedRelation: "educationalInstitutionTypes"
            referencedColumns: ["id"]
          },
        ]
      }
      educationalInstitutionTypes: {
        Row: {
          id: number
          name: string
        }
        Insert: {
          id?: number
          name: string
        }
        Update: {
          id?: number
          name?: string
        }
        Relationships: []
      }
      eventFormats: {
        Row: {
          id: number
          name: string
        }
        Insert: {
          id?: number
          name: string
        }
        Update: {
          id?: number
          name?: string
        }
        Relationships: []
      }
      eventReminderMessages: {
        Row: {
          eventId: number
          id: number
          message: string | null
          messageContentId: number
        }
        Insert: {
          eventId: number
          id?: number
          message?: string | null
          messageContentId: number
        }
        Update: {
          eventId?: number
          id?: number
          message?: string | null
          messageContentId?: number
        }
        Relationships: [
          {
            foreignKeyName: "event_reminder_messages_event_id_fkey"
            columns: ["eventId"]
            isOneToOne: false
            referencedRelation: "intellectualEvents"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "event_reminder_messages_message_content_id_fkey"
            columns: ["messageContentId"]
            isOneToOne: false
            referencedRelation: "messageContents"
            referencedColumns: ["id"]
          },
        ]
      }
      eventTeamApplicationMessages: {
        Row: {
          id: number
          message: string | null
          messageContentId: number
          teamApplicationId: number
        }
        Insert: {
          id?: number
          message?: string | null
          messageContentId: number
          teamApplicationId: number
        }
        Update: {
          id?: number
          message?: string | null
          messageContentId?: number
          teamApplicationId?: number
        }
        Relationships: [
          {
            foreignKeyName: "event_team_application_messages_message_content_id_fkey"
            columns: ["messageContentId"]
            isOneToOne: false
            referencedRelation: "messageContents"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "event_team_application_messages_team_application_id_fkey"
            columns: ["teamApplicationId"]
            isOneToOne: false
            referencedRelation: "teamsApplications"
            referencedColumns: ["id"]
          },
        ]
      }
      intellectualEventAddresses: {
        Row: {
          addressId: string
          eventId: number
          id: number
        }
        Insert: {
          addressId: string
          eventId: number
          id?: number
        }
        Update: {
          addressId?: string
          eventId?: number
          id?: number
        }
        Relationships: [
          {
            foreignKeyName: "intellectual_event_addresses_address_id_fkey"
            columns: ["addressId"]
            isOneToOne: false
            referencedRelation: "addresses"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "intellectual_event_addresses_event_id_fkey"
            columns: ["eventId"]
            isOneToOne: false
            referencedRelation: "intellectualEvents"
            referencedColumns: ["id"]
          },
        ]
      }
      intellectualEventFormats: {
        Row: {
          eventFormatId: number
          eventId: number
          id: number
        }
        Insert: {
          eventFormatId: number
          eventId: number
          id?: number
        }
        Update: {
          eventFormatId?: number
          eventId?: number
          id?: number
        }
        Relationships: [
          {
            foreignKeyName: "intellectual_event_formats_event_format_id_fkey"
            columns: ["eventFormatId"]
            isOneToOne: false
            referencedRelation: "eventFormats"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "intellectual_event_formats_event_id_fkey"
            columns: ["eventId"]
            isOneToOne: false
            referencedRelation: "intellectualEvents"
            referencedColumns: ["id"]
          },
        ]
      }
      intellectualEvents: {
        Row: {
          createdAt: string
          dateEnd: string | null
          dateStart: string
          description: string
          eventOrganizerId: number
          id: number
          maxTeamCount: number
          registrationFee: number | null
          title: string
        }
        Insert: {
          createdAt?: string
          dateEnd?: string | null
          dateStart: string
          description: string
          eventOrganizerId: number
          id?: number
          maxTeamCount?: number
          registrationFee?: number | null
          title: string
        }
        Update: {
          createdAt?: string
          dateEnd?: string | null
          dateStart?: string
          description?: string
          eventOrganizerId?: number
          id?: number
          maxTeamCount?: number
          registrationFee?: number | null
          title?: string
        }
        Relationships: [
          {
            foreignKeyName: "intellectual_events_event_organizer_id_fkey"
            columns: ["eventOrganizerId"]
            isOneToOne: false
            referencedRelation: "users"
            referencedColumns: ["id"]
          },
        ]
      }
      intellectualEventTeams: {
        Row: {
          createdAt: string
          eventId: number
          id: number
          isAccepted: boolean
          temporaryTeamId: number
        }
        Insert: {
          createdAt?: string
          eventId: number
          id?: number
          isAccepted?: boolean
          temporaryTeamId: number
        }
        Update: {
          createdAt?: string
          eventId?: number
          id?: number
          isAccepted?: boolean
          temporaryTeamId?: number
        }
        Relationships: [
          {
            foreignKeyName: "intellectual_event_teams_event_id_fkey"
            columns: ["eventId"]
            isOneToOne: false
            referencedRelation: "intellectualEvents"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "intellectual_event_teams_temporary_team_id_fkey"
            columns: ["temporaryTeamId"]
            isOneToOne: false
            referencedRelation: "temporaryTeams"
            referencedColumns: ["id"]
          },
        ]
      }
      messageContents: {
        Row: {
          id: number
          messageTypeId: number
        }
        Insert: {
          id?: number
          messageTypeId: number
        }
        Update: {
          id?: number
          messageTypeId?: number
        }
        Relationships: [
          {
            foreignKeyName: "message_contents_message_type_id_fkey"
            columns: ["messageTypeId"]
            isOneToOne: false
            referencedRelation: "messageTypes"
            referencedColumns: ["id"]
          },
        ]
      }
      messages: {
        Row: {
          createdAt: string
          id: number
          isRead: boolean
          messageContentId: number
          recipientId: number
        }
        Insert: {
          createdAt?: string
          id?: number
          isRead?: boolean
          messageContentId: number
          recipientId: number
        }
        Update: {
          createdAt?: string
          id?: number
          isRead?: boolean
          messageContentId?: number
          recipientId?: number
        }
        Relationships: [
          {
            foreignKeyName: "messages_message_content_id_fkey"
            columns: ["messageContentId"]
            isOneToOne: false
            referencedRelation: "messageContents"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "messages_recipient_id_fkey"
            columns: ["recipientId"]
            isOneToOne: false
            referencedRelation: "users"
            referencedColumns: ["id"]
          },
        ]
      }
      messageTypes: {
        Row: {
          id: number
          name: string
        }
        Insert: {
          id?: number
          name: string
        }
        Update: {
          id?: number
          name?: string
        }
        Relationships: []
      }
      playerStatusMessages: {
        Row: {
          id: number
          messageContentId: number
          teamId: string
        }
        Insert: {
          id?: number
          messageContentId: number
          teamId: string
        }
        Update: {
          id?: number
          messageContentId?: number
          teamId?: string
        }
        Relationships: [
          {
            foreignKeyName: "player_status_messages_message_content_id_fkey"
            columns: ["messageContentId"]
            isOneToOne: false
            referencedRelation: "messageContents"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "player_status_messages_team_id_fkey"
            columns: ["teamId"]
            isOneToOne: false
            referencedRelation: "teams"
            referencedColumns: ["id"]
          },
        ]
      }
      teamRecallStatusMessages: {
        Row: {
          id: number
          message: string | null
          messageContentId: number
          teamApplicationId: number
        }
        Insert: {
          id?: number
          message?: string | null
          messageContentId: number
          teamApplicationId: number
        }
        Update: {
          id?: number
          message?: string | null
          messageContentId?: number
          teamApplicationId?: number
        }
        Relationships: [
          {
            foreignKeyName: "team_recall_status_messages_message_content_id_fkey"
            columns: ["messageContentId"]
            isOneToOne: false
            referencedRelation: "messageContents"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "team_recall_status_messages_team_application_id_fkey"
            columns: ["teamApplicationId"]
            isOneToOne: false
            referencedRelation: "teamsApplications"
            referencedColumns: ["id"]
          },
        ]
      }
      teams: {
        Row: {
          createdAt: string
          id: string
          name: string
        }
        Insert: {
          createdAt?: string
          id?: string
          name: string
        }
        Update: {
          createdAt?: string
          id?: string
          name?: string
        }
        Relationships: []
      }
      teamsApplications: {
        Row: {
          createdAt: string
          id: number
          isCaptain: boolean
          isPlaying: boolean
          playerId: number
          temporaryTeamId: number
        }
        Insert: {
          createdAt?: string
          id?: number
          isCaptain?: boolean
          isPlaying?: boolean
          playerId: number
          temporaryTeamId: number
        }
        Update: {
          createdAt?: string
          id?: number
          isCaptain?: boolean
          isPlaying?: boolean
          playerId?: number
          temporaryTeamId?: number
        }
        Relationships: [
          {
            foreignKeyName: "teams_applications_player_id_fkey"
            columns: ["playerId"]
            isOneToOne: false
            referencedRelation: "users"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "teams_applications_temporary_team_id_fkey"
            columns: ["temporaryTeamId"]
            isOneToOne: false
            referencedRelation: "temporaryTeams"
            referencedColumns: ["id"]
          },
        ]
      }
      temporaryTeams: {
        Row: {
          createdAt: string
          id: number
          teamId: string
        }
        Insert: {
          createdAt?: string
          id?: number
          teamId: string
        }
        Update: {
          createdAt?: string
          id?: number
          teamId?: string
        }
        Relationships: [
          {
            foreignKeyName: "temporary_teams_team_id_fkey"
            columns: ["teamId"]
            isOneToOne: false
            referencedRelation: "teams"
            referencedColumns: ["id"]
          },
        ]
      }
      userNames: {
        Row: {
          createdAt: string
          firstName: string
          id: string
          patronymicName: string | null
          secondName: string
        }
        Insert: {
          createdAt?: string
          firstName: string
          id?: string
          patronymicName?: string | null
          secondName: string
        }
        Update: {
          createdAt?: string
          firstName?: string
          id?: string
          patronymicName?: string | null
          secondName?: string
        }
        Relationships: []
      }
      users: {
        Row: {
          authUserId: string | null
          birthDate: string
          createdAt: string
          id: number
        }
        Insert: {
          authUserId?: string | null
          birthDate: string
          createdAt?: string
          id?: number
        }
        Update: {
          authUserId?: string | null
          birthDate?: string
          createdAt?: string
          id?: number
        }
        Relationships: [
          {
            foreignKeyName: "users_auth_user_id_fkey"
            columns: ["authUserId"]
            isOneToOne: false
            referencedRelation: "users"
            referencedColumns: ["id"]
          },
        ]
      }
      usersEducationalInstitutions: {
        Row: {
          educationalInstitutionId: string
          id: number
          isCurrent: boolean
          userId: number
        }
        Insert: {
          educationalInstitutionId: string
          id?: number
          isCurrent?: boolean
          userId: number
        }
        Update: {
          educationalInstitutionId?: string
          id?: number
          isCurrent?: boolean
          userId?: number
        }
        Relationships: [
          {
            foreignKeyName: "usersEducationalInstitutions_educationalInstitutionId_fkey"
            columns: ["educationalInstitutionId"]
            isOneToOne: false
            referencedRelation: "educationalInstitutions"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "usersEducationalInstitutions_userId_fkey"
            columns: ["userId"]
            isOneToOne: false
            referencedRelation: "users"
            referencedColumns: ["id"]
          },
        ]
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}

type PublicSchema = Database[Extract<keyof Database, "public">]

export type Tables<
  PublicTableNameOrOptions extends
    | keyof (PublicSchema["Tables"] & PublicSchema["Views"])
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof (Database[PublicTableNameOrOptions["schema"]]["Tables"] &
        Database[PublicTableNameOrOptions["schema"]]["Views"])
    : never = never,
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? (Database[PublicTableNameOrOptions["schema"]]["Tables"] &
      Database[PublicTableNameOrOptions["schema"]]["Views"])[TableName] extends {
      Row: infer R
    }
    ? R
    : never
  : PublicTableNameOrOptions extends keyof (PublicSchema["Tables"] &
        PublicSchema["Views"])
    ? (PublicSchema["Tables"] &
        PublicSchema["Views"])[PublicTableNameOrOptions] extends {
        Row: infer R
      }
      ? R
      : never
    : never

export type TablesInsert<
  PublicTableNameOrOptions extends
    | keyof PublicSchema["Tables"]
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicTableNameOrOptions["schema"]]["Tables"]
    : never = never,
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? Database[PublicTableNameOrOptions["schema"]]["Tables"][TableName] extends {
      Insert: infer I
    }
    ? I
    : never
  : PublicTableNameOrOptions extends keyof PublicSchema["Tables"]
    ? PublicSchema["Tables"][PublicTableNameOrOptions] extends {
        Insert: infer I
      }
      ? I
      : never
    : never

export type TablesUpdate<
  PublicTableNameOrOptions extends
    | keyof PublicSchema["Tables"]
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicTableNameOrOptions["schema"]]["Tables"]
    : never = never,
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? Database[PublicTableNameOrOptions["schema"]]["Tables"][TableName] extends {
      Update: infer U
    }
    ? U
    : never
  : PublicTableNameOrOptions extends keyof PublicSchema["Tables"]
    ? PublicSchema["Tables"][PublicTableNameOrOptions] extends {
        Update: infer U
      }
      ? U
      : never
    : never

export type Enums<
  PublicEnumNameOrOptions extends
    | keyof PublicSchema["Enums"]
    | { schema: keyof Database },
  EnumName extends PublicEnumNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicEnumNameOrOptions["schema"]]["Enums"]
    : never = never,
> = PublicEnumNameOrOptions extends { schema: keyof Database }
  ? Database[PublicEnumNameOrOptions["schema"]]["Enums"][EnumName]
  : PublicEnumNameOrOptions extends keyof PublicSchema["Enums"]
    ? PublicSchema["Enums"][PublicEnumNameOrOptions]
    : never
