import { createApp } from 'vue';
import { createPinia } from 'pinia';

import App from './app/App.vue';
import router from './app/router';
import MasonryWall from '@yeger/vue-masonry-wall';

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(MasonryWall);

app.mount('#app');
