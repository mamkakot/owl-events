export const routes = [
  {
    path: '/',
    name: 'event_list',
    component: () => import('@/pages/intellectual-events'),
  },
  {
    path: '/event/:id',
    name: 'event',
    component: () => import('@/pages/intellectual-event'),
  },
  {
    path: '/user/:id',
    name: 'user',
    component: () => import('@/pages/personal-account'),
  },
  {
    path: '/user/:id/edit',
    name: 'edit-user',
    component: () => import('@/pages/edit-user-info'),
  },
  {
    path: '/create-event',
    name: 'create-event',
    component: () => import('@/pages/create-event'),
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('@/pages/register-user'),
    props: true,
  },
  {
    path: '/sign-in',
    name: 'sign-in',
    component: () => import('@/pages/sign-in'),
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: () => import('@/pages/register-user'),
  },
  {
    path: '/event-application',
    name: 'event-application',
    component: () => import('@/pages/event-application'),
  },
  {
    path: '/messages',
    name: 'messages',
    component: () => import('@/pages/user-messages'),
  },
  // {
  //   path: '/:pathMatch(.*)',
  //   component: () => import('@/pages/not-found'),
  //   meta: {
  //     layout: HomeLayout
  //   }
  // }
];