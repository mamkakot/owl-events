import { supabase } from '@/shared/api';
import type { UserData } from '@/features/registration-form';

export async function getInitialUserData(userId: string): Promise<UserData> {
  // Получение текущего имени и дня рождения пользователя.
  const getCurrentUserInfoResult = await supabase
    .from('allUserNames')
    .select(`
      userNames (
        firstName,
        secondName,
        patronymicName
      ),
      users (
        id,
        birthDate,
        iacId,
        aboutMe
      )
    `)
    .eq('userId', userId)
    .eq('isCurrent', 'true')
    .single();

  if (getCurrentUserInfoResult.error) {
    throw getCurrentUserInfoResult.error;
  }

  // Получение текущей команды.
  const getCurrentUserTeamResult = await supabase
    .from('basicTeamStructures')
    .select(`
      teams (name)
    `)
    .eq('playerId', userId)
    .maybeSingle();

  if (getCurrentUserTeamResult.error) {
    throw getCurrentUserTeamResult.error;
  }

  // Получение учебного заведения.
  const getCurrentUserEducationalInstitutionResult = await supabase
    .from('usersEducationalInstitutions')
    .select(`
      educationalInstitutions (
        name
      )
    `)
    .eq('userId', userId)
    .eq('isCurrent', 'true')
    .maybeSingle();

  if (getCurrentUserEducationalInstitutionResult.error) {
    throw getCurrentUserEducationalInstitutionResult.error;
  }

  const userData: UserData = {
    currentName: {
      firstName: getCurrentUserInfoResult.data.userNames!.firstName,
      secondName: getCurrentUserInfoResult.data.userNames!.secondName,
      patronymicName: getCurrentUserInfoResult.data.userNames!.patronymicName,
    },
    birthDate: getCurrentUserInfoResult.data.users!.birthDate,
    iacId: getCurrentUserInfoResult.data.users!.iacId,
    aboutMe: getCurrentUserInfoResult.data.users!.aboutMe,
    currentEducationalInstitution: {
      name: getCurrentUserEducationalInstitutionResult.data!.educationalInstitutions!.name,
    },
    password: '',
    email: '',
    teamName: getCurrentUserTeamResult.data?.teams?.name,
  };

  console.log(userData);
  return userData;
}