import PersonalAccount from './ui/PersonalAccount.vue';
import { getInitialUserData } from './api';

export default PersonalAccount;
export { getInitialUserData };