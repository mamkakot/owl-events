import EducationalInstitutionsField from './ui/EducationalInstitutionsField.vue';

export * as EducationalInstitutionsApi from './api';
export default EducationalInstitutionsField;