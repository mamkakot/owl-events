import { supabase } from '@/shared/api';

export async function getEducationalInstitutions() {
  const getEducationalInstitutionsNamesResult = await supabase
    .from('educationalInstitutions')
    .select('name');

  if (getEducationalInstitutionsNamesResult.error) {
    throw getEducationalInstitutionsNamesResult.error;
  }

  console.log(getEducationalInstitutionsNamesResult.data);

  return getEducationalInstitutionsNamesResult.data;
}