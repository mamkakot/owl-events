import { defineStore } from 'pinia';
import {
  IntellectualEventsApi,
  type IntellectualEventModel,
  type IntellectualEventListModel,
} from '@/entities/intellectual-event-list';

export const useIntellectualEventListModel = defineStore({
  id: 'intellectualEventList',

  state: () =>
    <IntellectualEventListModel>{
      intellectualEvents: [],
    },

  actions: {
    async getEvents(): Promise<void> {
      const items = await IntellectualEventsApi.getEvents();

      if (items) {
        this.updateIntellectualEvents(items);
      }
    },

    updateIntellectualEvents(eventModels: IntellectualEventModel[]): void {
      this.intellectualEvents = [...eventModels] || [];
    },
  },
});