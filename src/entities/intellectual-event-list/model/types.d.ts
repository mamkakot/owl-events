export type IntellectualEventModel = {
  createdAt: string;
  dateEnd: string | null;
  dateStart: string;
  description: string;
  eventOrganizerId: number;
  id: number;
  maxTeamCount: number;
  registrationFee: number | null;
  title: string;
}

export type IntellectualEventListModel = {
  intellectualEvents: IntellectualEventModel[];
}