import type { IntellectualEventModel, IntellectualEventListModel } from './model/types';

export * as IntellectualEventsApi from './api';
export type { IntellectualEventModel, IntellectualEventListModel };