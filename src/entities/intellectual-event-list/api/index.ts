import type { IntellectualEventModel } from '@/entities/intellectual-event-list/model/types';
import { supabase } from '@/shared/api';

export async function getEvents() {
  const { data: events, error } = await supabase
    .from('intellectualEvents')
    .select('*');

  return events as IntellectualEventModel[];
}

// export async function getEventById(id: number) {
//   const { data: events, error } = await supabaseClient
//     .from('intellectualEvents')
//     .select('*')
//     .eq('id', id);
//
//   return events![0] as IntellectualEventModel;
// }