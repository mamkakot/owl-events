import { supabase } from '@/shared/api';

export async function getTeams() {
  const getTeamsNamesResult = await supabase
    .from('teams')
    .select('name');

  if (getTeamsNamesResult.error) {
    throw getTeamsNamesResult.error;
  }

  return getTeamsNamesResult.data;
}