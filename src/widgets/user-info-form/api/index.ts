import { supabase } from '@/shared/api';
import type { UserData } from '@/features/registration-form';
import type { User } from '@supabase/supabase-js';

export async function editUserInfo(userData: UserData, currentUser: User) {
  const createUserNameResult = await supabase
    .from('userNames')
    .insert({
      firstName: userData.currentName.firstName,
      secondName: userData.currentName.secondName,
      patronymicName: userData.currentName.patronymicName,
    })
    .select();

  if (createUserNameResult.error) {
    throw createUserNameResult.error;
  }

  const createEducationalInstitutionResult = await supabase
    .from('educationalInstitutions')
    .insert({
      educationalInstitutionTypeId: 1,
      name: userData.currentEducationalInstitution!.name,
    })
    .select();

  if (createEducationalInstitutionResult.error) {
    throw createEducationalInstitutionResult.error;
  }

  const editUserResult = await supabase
    .from('users')
    .insert({
      authUserId: currentUser.id,
      birthDate: userData.birthDate.toLocaleDateString(),
    })
    .select();

  if (editUserResult.error) {
    throw editUserResult.error;
  }

  const createUserEducationalInstitutionResult = await supabase
    .from('usersEducationalInstitutions')
    .insert({
      userId: editUserResult.data[0].id,
      educationalInstitutionId: createEducationalInstitutionResult.data[0].id,
      isCurrent: true,
    });

  if (createUserEducationalInstitutionResult.error) {
    throw createUserEducationalInstitutionResult.error;
  }

  const createUserCurrentNameResult = await supabase
    .from('allUserNames')
    .insert({
      userId: editUserResult.data[0].id,
      userNameId: createUserNameResult.data[0].id,
      isCurrent: true,
    });

  if (createUserCurrentNameResult.error) {
    throw createUserCurrentNameResult.error;
  }
}

export async function getInitialUserData(authUser: User) {
  const getCurrentUserResult = await supabase.from('users')
    .select('id')
    .eq('authUserId', authUser.id)
    .single();

  if (getCurrentUserResult.error) {
    throw getCurrentUserResult.error;
  }

  // Получение текущего имени и дня рождения пользователя.
  const getCurrentUserInfoResult = await supabase
    .from('allUserNames')
    .select(`
      userNames (
        firstName,
        secondName,
        patronymicName
      ),
      users (
        id,
        birthDate,
        iacId,
        aboutMe
      )
    `)
    .eq('userId', getCurrentUserResult.data.id)
    .eq('isCurrent', 'true')
    .single();

  if (getCurrentUserInfoResult.error) {
    throw getCurrentUserInfoResult.error;
  }

  // Получение текущей команды.
  const getCurrentUserTeamResult = await supabase
    .from('basicTeamStructures')
    .select(`
      teams (name)
    `)
    .eq('playerId', getCurrentUserResult.data.id)
    .maybeSingle();

  if (getCurrentUserTeamResult.error) {
    throw getCurrentUserTeamResult.error;
  }

  // Получение учебного заведения.
  const getCurrentUserEducationalInstitutionResult = await supabase
    .from('usersEducationalInstitutions')
    .select(`
      educationalInstitutions (
        name
      )
    `)
    .eq('userId', getCurrentUserResult.data.id)
    .eq('isCurrent', 'true')
    .maybeSingle();

  if (getCurrentUserEducationalInstitutionResult.error) {
    throw getCurrentUserEducationalInstitutionResult.error;
  }

  const userData: UserData = {
    currentName: {
      firstName: getCurrentUserInfoResult.data.userNames!.firstName,
      secondName: getCurrentUserInfoResult.data.userNames!.secondName,
      patronymicName: getCurrentUserInfoResult.data.userNames!.patronymicName,
    },
    birthDate: getCurrentUserInfoResult.data.users!.birthDate,
    iacId: getCurrentUserInfoResult.data.users!.iacId,
    aboutMe: getCurrentUserInfoResult.data.users!.aboutMe,
    currentEducationalInstitution: {
      name: getCurrentUserEducationalInstitutionResult.data!.educationalInstitutions!.name,
    },
    password: '',
    email: authUser.email!,
    teamName: getCurrentUserTeamResult.data?.teams?.name,
  };

  console.log(userData);
  return userData;
}