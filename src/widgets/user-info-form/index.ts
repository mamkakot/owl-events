import UserInfoEdit from './ui/UserInfoForm.vue';
import { editUserInfo, getInitialUserData } from './api';

export default UserInfoEdit;
export { editUserInfo, getInitialUserData };