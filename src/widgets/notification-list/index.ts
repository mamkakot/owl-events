import UserNotifications from './ui/NotificationList.vue';
import type { NotificationModel, NotificationListModel } from './model/types';

export * as NotificationsApi from './api';
export type { NotificationModel, NotificationListModel };
export default UserNotifications;