import { supabase } from '@/shared/api';
import type { NotificationModel } from '../model/types';

export async function getNotifications(userId: number) {
  const getMessagesResult = await supabase
    .from('messageContents')
    .select(`
      messageTypes (
        name
      ),
      eventReminderMessages (
        eventId,
        message
      ),
      messages (
        recipientId,
        isRead
      )
    `)
    .eq('messages.recipientId', userId);

  if (getMessagesResult.error) {
    console.error(`sign up error: ${getMessagesResult.error}`);
    throw getMessagesResult.error;
  }

  const notification: NotificationModel = {
    isRead: getMessagesResult.data[0].messages[0].isRead,
    recipientId: getMessagesResult.data[0].messages[0].recipientId,
    message: getMessagesResult.data[0].eventReminderMessages[0]?.message ?? '',
    link: `/event/${getMessagesResult.data[0].eventReminderMessages[0].eventId}`,
  };

  return [notification];
}

