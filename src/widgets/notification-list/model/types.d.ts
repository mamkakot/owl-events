export type NotificationModel = {
  message?: string;
  link?: string;
  isRead: boolean;
  recipientId: number;
}

export type NotificationListModel = {
  notifications: NotificationModel[];
}