import { defineStore } from 'pinia';
import {
  NotificationsApi,
  type NotificationListModel,
  type NotificationModel,
} from '@/widgets/notification-list';

export const useNotificationListModel = defineStore({
  id: 'notificationList',

  state: () =>
    <NotificationListModel>{
      notifications: [],
    },

  actions: {
    async getNotifications(userId: number): Promise<void> {
      const items = await NotificationsApi.getNotifications(userId);

      if (items) {
        this.updateNotifications(items);
      }
    },

    updateNotifications(notificationModels: NotificationModel[]): void {
      this.notifications = [...notificationModels] || [];
    },
  },
});